# diagnostic-regional-etiages 1.1.0

* Connexion des données possible avec une bdd postgresql ou une bdd sqlite
* Adaptation des fonctions d'appels aux données (get)
* Mise à jour du descriptif des tables utilisées
* Mise à jour du README.md

# diagnostic-regional-etiages 1.0.1  
* internalisation des données dans le package  

# diagnostic-regional-etiages 1.0.0

### Module Cartographique

* Affichage par défaut du volet tabulaire
* Ajout d'un bandeau pour la définition de l'indicateur
* Correction des classes du module spécifique
 
### Module fiche station

* Ajout des définitions des indicateurs pour tous les graphiques
* Ajout de l'URL de la banque hydro de la station sélectionnée
* Ajout des définitions pour RRSE et 
* Ajout de fonctionnalités pour zoomer sur les graphiques VCN
* Amélioration toolip du graph sur les débits journaliers
* Ajout indicateur QMNA5 et module
* Correction coquilles et quelques modifications de contenu

---

# diagnostic-regional-etiages 0.9.0

### Fonctionnalité générale

* Organisation basée sur un menu des titres horizontal
* Refonte graphique basée sur la charte graphique d'Etat
* Amélioration de l'ergonomie et web-responsive
* Optimisation de la base de données
* Ajout d'un waiter pour le chargement   
* Ajout d'un glossaire
* Ajout des mentions légales
* Ajout d'un "a propos"

### Module Cartographique

* Refonte ergonomique pour une approche carto-centrée
* Ajout d'un renvoi entre la popup station et la fiche station
* Simplification du réseau hydrographique
* Ajout contour du réseau
* Ajout des stations non exploitées dans l’étude
* Ajout d'un bouton sur la carte pour revenir à l'échelle de la région
* Amélioration dans la construction des classes d'indicateurs
* Indicateur d'assc divisé en 2

### Module fiche station

* Refonte graphique et ergonomique
* Ajout dans l’en-tête de la typologie de l’étiage de la station
* Ajout des indicateurs de recession et de demi-vie
* Ajout d'un bouton pour changer les unités (l/s et m3/s)
* Ajout du réseau hydro dans l'emprise de la masse d'eau de la station

---

# diagnostic-regional-etiages 0.1.0

* Création de l'application beta
* Module cartographique avec les fonctionnalités de base pour explorer les résultats par indicateur
* Module de fiche station avec les fonctionnalités de base
