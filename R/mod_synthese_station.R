#' synthese_requete UI Function
#'
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#'
#' @noRd 
#' @import dplyr
#' @importFrom shiny NS tagList
#' @importFrom rlang .data
#' @import leaflet
#' 
mod_synthese_station_ui <- function(id){
  ns <- NS(id)
  
  tags$style(
        ".text-test{
          color: #f959ff !important;
      }"
    )

  tagList(
    fluidRow(
      class="top-action-bar",
      column(5,
        class="item",
        uiOutput(outputId = ns("uiout_choix_station"))
      )
    ),
    fluidRow(
      uiOutput(ns("fiche_station")),
      uiOutput(ns("contexte_station"))
    ),
    fluidRow(      
      column(7,
      class="tabContent-with-button",
        uiOutput(ns("uiout_choix_unite")),
        uiOutput(ns("table_indicateurs"))
      ),
      column(5,
        leaflet::leafletOutput(outputId = ns("map_station"), height = "862px")
      )
    )
  )
}

#' synthese_requete Server Function
#'
#' @noRd 
mod_synthese_station_server <- function(id, r){
  moduleServer( id, function(input, output, session){
 
  ns <- session$ns

  global <- reactiveValues(
    select_station = NULL # station sélectionnée
  )

  # Création du selectInput pour afficher la liste des stations ----
    output$uiout_choix_station <- renderUI({
      req(r$liste_station_suivi)

      tagList(
        shinyWidgets::pickerInput(inputId = ns("choix_station"), label = "S\u00e9lection d\'une station", choices = r$liste_station_suivi$label,
                                  width = "100%",
                                  options=shinyWidgets::pickerOptions(liveSearch=T)
                                  )
      )
  })

  # 
  observeEvent(input$choix_station,{
    req(input$choix_station)
    
    waiter_process(message="Chargement ..."," ...")

    global$select_station <- r$liste_all_station %>%
                              dplyr::filter(.data$label == input$choix_station) %>%
                              replace(is.na(.),"-")
    con <- get_con_bdd(r$dbinfo)
    
    global$massedeua <- get_polygon_hydrographie_geom(con,type_hydro="meso",cdbvspemdo=global$select_station$cdbvspemdo)
    global$episodes_assecs <- get_data_indicateur_interannuel(con,indicateur=c("nbr_jour","nbr_episode"),cdstation=global$select_station$cdstation)
    global$qmm_data <- get_data_indicateur_interannuel(con,indicateur="qmm",cdstation=global$select_station$cdstation)

    if(nrow(global$massedeua)>0){
      global$reseau_hydro_geom <- get_reseau_hydro_geom(con,poly_geom=sf::st_as_text(global$massedeua$geom))  
    }else{
      global$reseau_hydro_geom <- NULL
    }
    DBI::dbDisconnect(con)
    waiter::waiter_hide()
  })

  output$uiout_choix_unite <- renderUI({
    shinyWidgets::radioGroupButtons(
         inputId = ns("choix_unite"),
         label = "Unit\u00e9 du d\u00e9bit",
         choices = c("l/s", "m3/s"),
         direction = "horizontal",
         status = "primary"
         )
  })

  output$uiout_definition_rrse <- renderUI({ 
        choix_indicateur <- "rrse"
        definition <- r$definitions[id %in% choix_indicateur,]
        dropMenu_information(id=ns("def_rrse"),label="",titre=definition$titre,definition=definition$definition)

  })

  output$uiout_definition_severite_etiage <- renderUI({ 
        choix_indicateur <- "severite_etiages"
        definition <- r$definitions[id %in% choix_indicateur,]
        dropMenu_information(id=ns("def_severite_etiages"),label="",titre=definition$titre,definition=definition$definition)
  })

  # Fiche d'identité de la station
  output$fiche_station <- renderUI({
    req(input$choix_station,global$select_station)
        
    titleStation <- a(input$choix_station, href=paste0("https://hydro.eaufrance.fr/sitehydro/",global$select_station$cdstation,"/fiche"),target="_blank")

    date_extraction_donnees <- paste("<td class=\'item\'><span class=\'label\'>Extraction des donn\u00e9es</span><div class=\'info-value\'>",r$date_extraction_donnees,'</div></td>')
    
    if(global$select_station$surface_finale!="-"){
      superficie_bv <- paste("<td class=\'item\'><span class=\'label\'>Superficie r\u00e9\u00e9lle</span><div class=\'info-value\'>",round(as.numeric(global$select_station$surface_finale))," km2",'</div></td>')
    }else{
      superficie_bv <- paste("<td class=\'item\'><span class=\'label\'>Superficie r\u00e9\u00e9lle</span><div class=\'info-value\'>-</div>",'</td>')
    }
    
    RRSE_def <- HTML(as.character(uiOutput(ns("uiout_definition_rrse"))))
    severite_etiage_def <- HTML(as.character(uiOutput(ns("uiout_definition_severite_etiage"))))
    
    RRSE <- paste("<td class=\'item\'><span class=\'label\'>RRSE</span>",RRSE_def,"<div class=\'info-value\'>",global$select_station$RRSE,'</div></td>')
    finalite_station <- paste("<td class=\'item\'><span class=\'label\'>Finalit\u00e9</span><div class=\'info-value\'>",global$select_station$finalite_station,'</div></td>')
    influence_station <- paste("<td class=\'item\'><span class=\'label\'>Influence</span>","<div class=\'info-value\'>",global$select_station$influence,'</div></td>')
    typologie_severite <- paste("<td class=\'item\'><span class=\'label\'>S\u00e9v\u00e9rit\u00e9 de l\'\u00e9tiage</span>",severite_etiage_def,"<div class=\'info-value\'>",global$select_station$typologie_severite,'</div></td>')

    userBox(width=6,
        id="desc-station",
        title = userDescription(
          title = HTML(paste0("Station ",titleStation)),
          subtitle = HTML( paste('<table class=\"table-item\"><tr>',paste0(c(date_extraction_donnees,superficie_bv,RRSE,finalite_station,influence_station,typologie_severite),collapse=""),'</tr></table>')),
          type = 2,
          image = "www/icons/station.svg"
        ),
        status = "primary",
        collapsible=FALSE,
        column(6,
          navPills(
            navPillsItem(
              left = "Mise en service", 
              color = "black",
              right = format(as.Date(global$select_station$date_mservice),"%d/%m/%Y")
            ),
            navPillsItem(
              left = "Emprise temporelle", 
              color = "black",
              right = paste0(global$select_station$emprise_temp_annee," ann\u00e9es")
            ),
            navPillsItem(
              left = "P\u00e9riode analys\u00e9e", 
              color = "black",
              right = paste0(global$select_station$annee_debut," - ",global$select_station$annee_fin)
            )
          )
        ),
        column(6,
          navPills(
            navPillsItem(
              left = "Fiabilit\u00e9 des donn\u00e9es basses eaux", 
              color = "black",
              right = global$select_station$qualite_basses_eaux,
            ),
            navPillsItem(
              left = "Fiabilit\u00e9 des donn\u00e9es moyennes eaux", 
              color = "black",
              right = global$select_station$qualite_moy_eaux
            ),
            navPillsItem(
              left = "Fiabilit\u00e9 des donn\u00e9es hautes eaux", 
              color = "black",
              right = global$select_station$qualite_hautes_eaux
            )
          )
        )
    )
  })
  
  # Contexte de la station
  output$contexte_station <- renderUI({
    req(input$choix_station,global$select_station)

    if(nrow(global$massedeua)>0){
      title_meso <- paste0("Masse d\'eau ",global$select_station$cdbvspemdo," : ",stringr::str_to_title(tolower(global$select_station$nombvspemd)))  
    }else{
      title_meso <- "Abscence d\'information sur la masse d\'eau de cette station"
    }

    userBox(width=6,
        id="desc-me",
        title = userDescription(
          title = title_meso,
          subtitle = "",
          type = 2,
          image = "www/icons/masse-eau.svg"
        ),
        status = "primary",
        collapsible=FALSE,
        column(6,
          navPills(
            navPillsItem(
              left = "\u00c9tat des lieux", 
              color = "black",
              right = "2019"
            ),
            navPillsItem(
              left = "\u00c9tat \u00e9cologique", 
              color = "black",
              right = global$select_station$ecolo_etat_label
            ),
            navPillsItem(
              left = "\u00c9tat chimique", 
              color = "black",#ifelse(global$select_station$chim_etat=="3","#AF1E20",ifelse(global$select_station$chim_etat=="2"),"blue","grey"),
              right = global$select_station$chim_etat_label
            )
          )
        ),
        column(6,
          navPills(
            navPillsItem(
              left = "Pression hydrologique", 
              color = ifelse(global$select_station$r_hydro=="1","#AF1E20","#45B170"),
              right = global$select_station$r_hydro_label
            ),
            navPillsItem(
              left = "Pression morphologique", 
              color = ifelse(global$select_station$r_morpho=="1","#AF1E20","#45B170"),
              right = global$select_station$r_morpho_label
            ),
            navPillsItem(
              left = "Pression pollution diffuse", 
              color = ifelse(global$select_station$r_pdiff=="1","#AF1E20","#45B170"),
              right = global$select_station$r_pdiff_label
            )
          )
        )
    )
  })

  output$table_pression_me <- DT::renderDataTable({
    global$select_station %>% as_tibble %>%
      dplyr::select(.data$r_hydro, .data$r_morpho, .data$r_pdiff) %>% 
      DT::datatable(
          rownames = FALSE,
          colnames = c("r_hydro","r_morpho","r_pdiff"),
          class = 'cell-border stripe',
          selection = "none",
          extensions = c("Buttons"),
          options = list(
            "scrollX" = TRUE,
            "searching" = FALSE,
            "paging" = FALSE, 
            "lengthChange" = FALSE, 
            "info" = FALSE,
            "ordering" = FALSE,
            columnDefs = list(
              list(
                className = 'dt-center',
                targets = "_all")
            )
          )
      )
  })

  output$table_indicateurs <- renderUI({
    tagList(
      tags$div(
        class="table-title-definition", 
        tabBox(
          width = 12,
          side = "left",
          title = "",
          tabPanel("Hydrologie g\u00e9n\u00e9rale",
            uiOutput(ns("uiout_valeur_module")),
            tags$hr(class="sep"),
            uiOutput(ns("uiout_definition_qmnj")),
            echarts4r::echarts4rOutput(outputId = ns("chart_qmmj"), height = "400px"),
            tags$hr(class="sep"),
            uiOutput(ns("uiout_definition_qmm")),
            echarts4r::echarts4rOutput(outputId = ns("chart_qmm"), height = "400px")
          ),
          tabPanel("Hydrologie d\'\u00e9tiage",
            uiOutput(ns("uiout_valeur_qmna5")),
            tags$hr(class="sep"),
            uiOutput(ns("uiout_definition_qmna")),
            echarts4r::echarts4rOutput(outputId = ns("chart_qmna"), height = "400px"),
            tags$hr(class="sep"),
            uiOutput(ns("uiout_definition_ie")),
            echarts4r::echarts4rOutput(outputId = ns("chart_intensite_etiage"), height = "400px"),
            tags$hr(class="sep"),
            uiOutput(ns("uiout_definition_vcn")),
            echarts4r::echarts4rOutput(outputId = ns("chart_vcn"), height = "400px"),
            tags$hr(class="sep"),
            uiOutput(ns("uiout_definition_recession")),
            fluidRow(
              align="center",
              column(6,
                echarts4r::echarts4rOutput(outputId = ns("chart_indicateur_recession"), height = "400px")  
              ),
              column(6,
                echarts4r::echarts4rOutput(outputId = ns("chart_demie_vie"), height = "400px")  
              ),
            ),
          ),
          tabPanel("Ass\u00e8chement",
            uiOutput(ns("uiout_definition_assechement")),
            echarts4r::echarts4rOutput(outputId = ns("chart_episodes_assecs"), height = "400px")
          )
        )
      )
    )
  })

  # Partie carte ####
  output$map_station <- leaflet::renderLeaflet({
    req(global$select_station)
    
    content_pop <- paste0("
          <b> Nom de la station <b/>: ",global$select_station$nom_station,"<br>
          <b> Code de la station <b/>: ",global$select_station$cdstation
    )

    map <- map_init()
    
    if(nrow(global$massedeua)>0){
      map <- map %>%
             map_add_polygon_hydro(global$massedeua,groupe="Contour de la massse d\'eau") %>%
             map_add_reseau_hydro(reseau_hydro=global$reseau_hydro_geom,"R\u00e9seau hydrographique") %>%
             leaflet::addLayersControl(
                                        baseGroups = c("Open Street Map - CARTO","Open Street Map","Image satellite"),
                                        overlayGroups = c("Contour de la massse d\'eau"),
                                        options = leaflet::layersControlOptions(collapsed = FALSE),position = "topright")
    }
    map %>%
        leaflet::addCircleMarkers(
        data = global$select_station,
        group = "select_station",
        radius = 5,
        weight = 1,
        fillOpacity = 1,
        fillColor = "#D8AD88",
        color = "black",
        label = ~label,
        popup = content_pop
        )
  })

  output$info_hydrologie_etiage <- renderUI({
    con <- get_con_bdd(r$dbinfo)
    QMNA5 <- get_data_indicateur(con=con, table_indicateur="diagnostic_regional_etiages.qmna5_sec", cdstation=global$select_station$cdstation)
    DBI::dbDisconnect(con)

    QMNA5$val_th
  })

  # Partie graphiques ####

  # Hydrologie générale ####

  output$uiout_valeur_module <- renderUI({
    req(input$choix_unite)
    if(input$choix_unite=="l/s"){
        module <- global$module_QA*1000
        unite <- "l/s"
    }else{
      module <- global$module_QA
      unite <- "m3/s"
      digits <- 2
    }

    titre <- paste0("D\u00e9bit moyen interannuel (module) : ",module," ",unite)
    titre
  })

  output$uiout_definition_qmnj <- renderUI({
    choix_indicateur <- "qmnj"
    definition <- r$definitions[id %in% choix_indicateur,]
    dropMenu_information(id=ns("def_qmnj"),label=definition$titre,titre=definition$titre,definition=definition$definition)
  })

  output$chart_qmmj <- echarts4r::renderEcharts4r({
    con <- get_con_bdd(r$dbinfo)

    if(class(con)[1]=="SQLiteConnection"){
      table_indicateur <- "diagnostic_regional_etiages__module"
    }else{
      table_indicateur <- "diagnostic_regional_etiages.module"
    }

    QA <- get_data_indicateur(con=con,table_indicateur=table_indicateur,cdstation=global$select_station$cdstation)
    global$module_QA <- QA$module_QA
    data_chart <- global$qmm_data
    DBI::dbDisconnect(con)
    
    chart_qmmj(id_chart=ns("chart_qmmj"),data_chart=data_chart,module_qa=global$module_QA,unite_debit=input$choix_unite)
  })


  output$uiout_definition_qmm <- renderUI({
    choix_indicateur <- "qmm"
    definition <- r$definitions[id %in% choix_indicateur,]
    dropMenu_information(id=ns("def_qmm"),label=definition$titre,titre=definition$titre,definition=definition$definition)
  })

  # Régime mensuel annuel (QMM) #### 
  output$chart_qmm <- echarts4r::renderEcharts4r({
    mois_fr <- c("Jan", "Fevr", "Mars", "Avr", "Mai", "Juin", "Juill", "Aout", "Sept", "Oct", "Nov", "Dec")
    data_chart <- global$qmm_data %>%
                  dplyr::group_by(.data$mois) %>%
                  dplyr::summarise(debit=round(mean(.data$valeur, na.rm=TRUE),2)) %>%
                  dplyr::arrange(.data$mois) %>%
                  dplyr::mutate(mois = mois_fr[as.numeric(mois)])
    
    chart_qmm(id_chart=ns("chart_qmm"),data_chart, qa=global$module_QA,unite_debit=input$choix_unite)
  })

  # Hydrologie d'étiage ####
  
  output$uiout_valeur_qmna5<- renderUI({
    req(input$choix_unite,global$qmna5)

    if(input$choix_unite=="l/s"){
        qmna5 <- round(global$qmna5$valeur*1000,1)
        unite <- "l/s"
    }else{
      qmna5 <- round(global$qmna5$valeur,4)
      unite <- "m3/s"
    }

    titre <- paste0("D\u00e9bit mensuel minimal annuel sur 5 ans (QMNA5) : ",qmna5," ",unite)
    titre
  })


  output$uiout_definition_qmna <- renderUI({ 
      choix_indicateur <- "D\u00e9bit mensuel minimal annuel (QMNA)"
      definition <- unique(r$liste_indicateur[nom_indicateur_global %in% choix_indicateur & type_representation %in% c("cartographique_annuel","cartographique_globale"), definition])
      dropMenu_information(id=ns("def_qmna"),label=choix_indicateur,titre=choix_indicateur,definition)

  })

  # QMNA #### 
  output$chart_qmna <- echarts4r::renderEcharts4r({
    con <- get_con_bdd(r$dbinfo)

    data_chart <- get_data_indicateur_interannuel(con,indicateur="qmna",cdstation=global$select_station$cdstation) %>%
                    dplyr::mutate(annee=as.factor(.data$annee)) %>%
                    dplyr::arrange(.data$annee)

    global$qmna5 <- get_data_indicateur_station(con,indicateur="qmna5",cdstation=global$select_station$cdstation)
    DBI::dbDisconnect(con)

    chart_qmna(id_chart=ns("chart_qmna"),data_chart,qmna5=round(global$qmna5$valeur,4),unite_debit=input$choix_unite)
  })

  output$uiout_definition_ie <- renderUI({  
      choix_indicateur <- "Intensit\u00e9 d\u2019\u00e9tiage"
      definition <- unique(r$liste_indicateur[nom_indicateur_global %in% choix_indicateur & type_representation %in% c("cartographique_annuel","cartographique_globale"), definition])
      dropMenu_information(id=ns("def_ie"),label=choix_indicateur,titre=choix_indicateur,definition)
  })

  # Intensité d'étiage ####
  output$chart_intensite_etiage <- echarts4r::renderEcharts4r({

    con <- get_con_bdd(r$dbinfo)

    data_chart <- get_data_indicateur_interannuel(con,indicateur="intensite_etiage2", cdstation=global$select_station$cdstation) %>%
                    dplyr::mutate(annee=as.factor(.data$annee))

    DBI::dbDisconnect(con)

    chart_intensite_etiages_interannuel(id_chart=ns("chart_intensite_etiage"),data_chart,meam_intensite=round(mean(data_chart$valeur,na.rm=TRUE),4))
  })

  output$uiout_definition_recession <- renderUI({  
      div(class="label-info-icon",
       div(class="labelI","Analyse de la courbe de r\u00e9cession"),
        shinyWidgets::dropMenu(tag=actionLink(ns("description_recession"),icon("info")),
               tags$div(
                  tags$h4("Indicateurs associ\u00e9e \u00e0 la courbe de r\u00e9cession"),
                  hr(),
                  tags$ul(
                    tags$li(
                      HTML(
                      '<b>Demie vie :</b> Indicateur extrait de l\u2019analyse de la courbe de r\u00e9cession <a href=\"https://hal.inrae.fr/tel-02597804/document\" target=\"_blank\">C. Catalogne,2012</a>. Il indique le temps n\u00e9cessaire pour parcourir la moiti\u00e9 de la pente (vitesse de la premi\u00e8re phase de r\u00e9cession). En d\u2019autres termes, il renseigne sur la vitesse de vidange des stocks hydriques d\u2019un hydrosyst\u00e8me.'),
                      ),
                    tags$li(
                      HTML(
                        '<b>Coefficient de r\u00e9cession :</b> Indicateur issu de la m\u00e9thode d\u2019analyse de la courbe de r\u00e9cession propos\u00e9e par C. Catalogne (2012) qui repr\u00e9sente la r\u00e9silience d\u2019un bassin versant en l\u2019absence de pr\u00e9cipitation. Plus sa valeur est faible, plus le bassin est r\u00e9silient. A l\u2019inverse, une valeur \u00e9lev\u00e9e indiquera une sensibilit\u00e9 du bassin en p\u00e9riode de s\u00e9cheresse. Cet indicateur s\u2019analyse conjointement avec le temps de demi-vie d\u00e9finit plus bas.'
                      )
                    )
                  )
                ),
                placement = "bottom-start",arrow = TRUE,maxWidth=400)
        )

  })

  # Gauge pour la demie-vie ####
  output$chart_demie_vie <- echarts4r::renderEcharts4r({
    chart_indicateur_recession(data_chart=r$data_indicateur,cdstation=global$select_station$cdstation,indicateur="coefficient_recession")
  })

  # Gauge pour le coefficient_recession ####
  output$chart_indicateur_recession<- echarts4r::renderEcharts4r({
    chart_indicateur_recession(data_chart=r$data_indicateur,cdstation=global$select_station$cdstation,indicateur="demie_d")
  })

  output$uiout_definition_vcn <- renderUI({  
      choix_indicateur <- "Volume Cons\u00e9cutif Minimal (VCN)"
      definition <- unique(r$liste_indicateur[nom_indicateur_global %in% choix_indicateur & type_representation %in% c("cartographique_annuel","cartographique_globale"), definition])
      dropMenu_information(id=ns("def_vcn"),label=choix_indicateur,titre=choix_indicateur,definition)
  })

  # VCN #### 
  output$chart_vcn <- echarts4r::renderEcharts4r({
    con <- get_con_bdd(r$dbinfo)
    vcn_data <- get_data_indicateur_interannuel(con,indicateur="vcn",cdstation=global$select_station$cdstation) %>%
                dplyr::arrange(.data$vcn, .data$annee, .data$mois, .data$jour) %>%
                dplyr::mutate(date = lubridate::ymd(paste0(.data$annee, "/", .data$mois, "/", .data$jour)),
                              vcn=factor(.data$vcn, levels = c("vcn3","vcn7","vcn10","vcn30"))) 
    DBI::dbDisconnect(con)

    chart_vcn(id_chart=ns("chart_vcn"),vcn_data,unite_debit=input$choix_unite)             
  })

  # Assèchement ####
  output$uiout_definition_assechement <- renderUI({  
        choix_indicateur <- "assechement"
        definition <- r$definitions[id %in% choix_indicateur,]
        dropMenu_information(id=ns("def_assechement"),label="Nombre de jours et d\'\u00e9pisode d\'assec",titre=definition$titre,definition=definition$definition)

  })

  # Graph episodes assecs
  output$chart_episodes_assecs <- echarts4r::renderEcharts4r({
    chart_episodes_assecs(id_chart=ns("chart_episodes_assecs"),data_chart=global$episodes_assecs)
  })

  })
}
    
## To be copied in the UI
# synthese_station_ui("synthese_station_ui_1")
    
## To be copied in the server
# callModule(mod_synthese_station_server, "synthese_station_ui_1")
 
