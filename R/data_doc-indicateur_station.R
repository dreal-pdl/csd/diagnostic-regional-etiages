#' indicateur_station
#'
#' Description.
#'
#' @format A data frame with 444 rows and 3 variables:
#' \describe{
  #'   \item{ cdstation }{  character }
  #'   \item{ valeur }{  numeric }
  #'   \item{ indicateur }{  character }
  #' }
  #' @source Source
  "indicateur_station"
