#' @title chart_qma
#' @description Construction d'un graphique pour representer le QMA
#' @param id_chart id de l'output
#' @param data_chart Donnees utiles pour representer le QMA
#' @param unite_debit choix de l'unite du debit : m3/s ou l/s
#' @param show_title Booleen, afficher ou non le titre dans le graph 
#' @return plot echarts4r
#' @import echarts4r
#' @import dplyr
#' @export
#' @source https://github.com/JohnCoene/echarts4r/issues/99
#' 
chart_qma <- function(id_chart,data_chart,unite_debit="m3/s",show_title=FALSE){
    
    if(unite_debit=="l/s"){
        data_chart <- data_chart %>%
                        dplyr::mutate(valeur=.data$valeur*1000)
        unite <- "l/s"
        digits <- 0
    }else{
        unite <- "m3/s"
        digits <- 2
    }
      
    title <- "D\u00e9bit Moyen Annuel (QMA)"
    data_chart %>%
        echarts4r::e_charts(annee) %>%
        echarts4r::e_title(text = title,show=show_title) %>%
        echarts4r::e_bar(serie = valeur, name="D\u00e9bit",color="#000091") %>%
        echarts4r::e_legend(FALSE) %>%
        echarts4r::e_y_axis(name = paste0("D\u00e9bit (",unite,")"), nameLocation = "middle", nameGap = 50,
          formatter = e_axis_formatter("decimal", locale = "fr",digits = digits)) %>%
        echarts4r::e_toolbox_feature(
          feature = c("mysaveAsImage"),
          title = "T\u00e9l\u00e9charger",
          icon="M4.7,22.9L29.3,45.5L54.7,23.4M4.6,43.6L4.6,58L53.8,58L53.8,43.6M29.2,45.1L29.2,0",
          onclick=htmlwidgets::JS(title_chart_save(id=id_chart,titre=title))
           ) %>%
        echarts4r::e_tooltip(
          trigger = "item",
          formatter = htmlwidgets::JS(paste0("
                                    function(params){
                                      if (params.value.length>1){
                                        var val = parseFloat((params.value[1] * 10) / 10).toFixed(2)
                                        var label = params.marker + params.name + '<br>' +
                                                    '<b>D\u00e9bit :</b> ' + val + ' ",unite,"';
                                      }else{
                                        var val = params.value;
                                        var label = '<b>Module :</b> ' + val + ' ",unite,"';
                                      }

                                    return(label)
                                    }"))
        )

}

                 
