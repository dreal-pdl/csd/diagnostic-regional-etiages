#' @title get_data_indicateur
#' @description Extraction de la liste des indicateurs
#' @param con Parametre de connexion d'un DBI::dbConnect
#' @param table_indicateur nom de la table de l'indicateur
#' @param col_indicateur_date nom des colonnes dates
#' @param col_indicateur_valeur nom des colonnes des valeurs
#' @param cdstation code d'une station
#' @importFrom glue glue_sql
#' @import data.table
#' @return objet data.table
#' @export
#' 

get_data_indicateur <- function(con,table_indicateur, col_indicateur_date=NULL,col_indicateur_valeur=NULL,cdstation=NULL){

  if(is.null(cdstation)){
    
    if(col_indicateur_date==""){
      col_indicateur_date <- NULL
    }
    if(col_indicateur_valeur==""){
      col_indicateur_valeur <- NULL
    }

    if(is.null(col_indicateur_date) & is.null(col_indicateur_valeur)){
      query <- glue::glue("SELECT *
                                FROM {table_indicateur}",.con=con)
    }else if(!is.null(col_indicateur_date) & is.null(col_indicateur_valeur)){
      col_indicateur <- unlist(strsplit(col_indicateur_date,"-"))

      query <- gsub("'", '',glue::glue_sql("SELECT cdstation,{`col_indicateur`*}
                               FROM {table_indicateur}",.con=con))
    }else if(is.null(col_indicateur_date) & !is.null(col_indicateur_valeur)){
      col_indicateur <- unlist(strsplit(col_indicateur_valeur,"-"))

      query <- gsub("'", '',glue::glue_sql("SELECT cdstation,{`col_indicateur`*}
                               FROM {table_indicateur}",.con=con))
    }else{
      col_indicateur_date <- unlist(strsplit(col_indicateur_date,"-"))
      col_indicateur_valeur <- unlist(strsplit(col_indicateur_valeur,"-"))

      query <- gsub("'", '',glue::glue_sql("SELECT cdstation,{`col_indicateur_date`*},{`col_indicateur_valeur`*}
                               FROM {table_indicateur}",.con=con))
    }
  }else{
    query <- glue::glue("SELECT *
                         FROM {table_indicateur}
                         WHERE cdstation IN ('{cdstation}')",.con=con)
  }
  
  data_query <- data.table::setDT(DBI::dbGetQuery(con,query))

  return(data_query)
}