# dataviz-influence-etiage

<!-- badges: start -->
[![Lifecycle: stable](https://img.shields.io/badge/lifecycle-stable-green.svg)](https://www.tidyverse.org/lifecycle/#stable)
<!-- badges: end -->

Application de visualisation de données des résultats de l'étude Diagnostic Régional des Étiages en région Pays-de-la-Loire. L'étude et son application ont été réalisées par la [DRI d'Antea Group](https://www.anteagroup.fr/services/gestion-donnees-environnementales-data).

## Base de données

L'application peut s'appuyer sur une base de données PostgreSQL/PostGIS (Version 13) ou sqlite de travail associée à l'étude

## Dépendances

- Liste des packages utilisés :

```r
> attachment::att_from_rscripts()
 [1] "config"       "sf"           "dplyr"        "DBI"          "shinyjs"     
 [6] "waiter"       "classInt"     "forcats"      "leaflet"      "stringr"     
[11] "echarts4r"    "tidyr"        "lubridate"    "RPostgres"    "glue"        
[16] "data.table"   "leaflegend"   "DT"           "shinyWidgets" "htmltools"   
[21] "tibble"       "shiny"        "base64enc"
```

- Installation des packages nécessaires :

```r
install.packages("attachment")
attachment::install_from_description()
```

## Carnet de développement

L'application est développée en R-Shiny avec le package [golem](https://github.com/ThinkR-open/golem). Les différentes parties de l'application sont organisées par modules et les fonctionnalités métiers de l'application sont développées au sein de fonctions thématiques.

### Accès au code

- Clone du projet

``` bash
git clone https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/diagnostic-regional-etiages.git
```

- Cloner une branche

``` bash
git clone --branch unebranche https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/diagnostic-regional-etiages.git
```

- Configuration du remote 

``` bash
# Supprimer le remote d'origine
git remote rm origin
# Ajouter le nouveau remote avec votre {TOKEN}
git remote add origin \
"https://forge_dev_durable:{TOKEN}@gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/diagnostic-regional-etiages.git"
```

D'une manière générale, les objets, fonctions et fichiers sont nommés en `snake_case`.
    

### Convention de nommage des fichiers R

```
- [mod]_[nom_du_module] : module shiny
- Les autres types de fichiers sont nommés par le nom de la fonction
```

### Convention de nommage des fonctions

```
- [map]_[une_fonctionnalite] : Création d'une carte
- [tab]_[une_fonctionnalite] : Création d'une table
- [calc]_[une_fonctionnalite] : Fonction de calcul
- [chart]_[une_fonctionnalite] : Création d'un graphique
- [get]_[nom_de_la_table] : Pour récupérer les données d'une table
```



## Déploiement

#### En local, dans l'environnement de développement

1. Configurer la connexion vers la base de données dans le fichier `R/run_dev` :

```r
# Connexion à une base de données postgresql
conf_db <- list(dbname="dbname",
                host = "host",
                port = "port",
                user = "user",
                password = "password")

# Connexion à une base de données spatialite
conf_db <- "/chemin/baseDeDonnees.sqlite"

```

2. A la racine du projet, lancer la commande suivante :

```r
golem::run_dev()
```

### En local, en installant l'application

A la racine du projet :

1. Installer le paquet en local : 

``` r
remotes::install_local(upgrade="never")
```

2. Lancer à la racine du projet les commandes suivantes :

``` r
# Connexion à une base de données postgresql
conf_db <- list(dbname="dbname",
                host = "host",
                port = "port",
                user = "user",
                password = "password")

# Connexion à une base de données spatialite
conf_db <- "/chemin/baseDeDonnees.sqlite"

# Lancemement de l'application
datavizinfluenceetiage::run_app(conf_db=conf_db)
```

### En local, dans une image docker

- Construction du docker file :

```r
attachment::att_amend_desc()
golem::add_dockerfile()
```

- Configuration :

```bash
# Construction du docker
docker build . -t datavizinfluenceetiage

# Lancer le docker run avec les variables d'environnement de connexion à la BDD
docker run \
-e DB_NAME='DB_NAME' \
-e DB_HOST='IP' \
-e DB_PORT='PORT' \
-e DB_USER='DB_USER' \
-e DB_PW='PW' \
--name datavizinfluenceetiage 
```

### Déploiement sur un serveur shinyProxy

- Configuration pour le 1er déploiement

```bash
# Construction du docker dans 
cd ~/
git clone https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/diagnostic-regional-etiages.git
cd ~/diagnostic-regional-etiages

sudo docker build . -t datavizinfluenceetiage

# Ajouter l'application dans l'application.yml
sudo vim ~/shinyproxy_kit/shinyproxy/application.yml

- id: datavizinfluenceetiage
    display-name: ""
    description: ""
    container-cmd: ["R", "-e", "options('shiny.port'=3838,shiny.host='0.0.0.0');conf_db <- list(dbname = Sys.getenv('DB_NAME'),
                                        host = Sys.getenv('DB_HOST'),
                                        port =Sys.getenv('DB_PORT'),
                                        user = Sys.getenv('DB_USER'),
                                        password = Sys.getenv('DB_PW'));datavizinfluenceetiage::run_app(conf_db=conf_db)"]
    container-image: datavizinfluenceetiage
    container-network: sp-example-net
    container-volumes: ["/var/log/shinyproxy:/var/log/shinyproxy"]
    container-env:
        DB_NAME: *******
        DB_HOST: *******
        DB_PORT: *******
        DB_USER: *******
        DB_PW: *******

# Ajout d'une vignette
cp ~/shinyproxy_kit/apps/diagnostic-regional-etiages/inst/extdata/datavizinfluenceetiage.png ~/shinyproxy_kit/shinyproxy/templates/2col/assets/img

# Redémarrer shinyproxy
cd ~/shinyproxy_kit/shinyproxy/
docker-compose stop
docker-compose build shinyproxy
docker-compose up -d
```

- Mise à jour de l'application

```bash
cd ~/shinyproxy_kit/shinyproxy/apps/
git pull origin main

docker build . -t datavizinfluenceetiage
```
