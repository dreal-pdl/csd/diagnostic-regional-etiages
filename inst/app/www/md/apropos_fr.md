<div class="page-title"><h1>À propos de l'application Diagnostic Régional Étiages</h1></div>

<div class="col-sm-12 pad">

<h2>L'application</h2>

Cette application a été développée par la <a href="https://www.anteagroup.fr/services/gestion-donnees-environnementales-data DRI d'Antea Group" target="_blank"> DRI d'Antea Group</a> dans le cadre de l'étude sur la sévérité des étiages de la Région Pays de la Loire réalisée sous maitrise d’ouvrage de la DREAL Pays de la Loire sur la période 2020-2022. Elle permet d'explorer de manière interactive les différents indicateurs hydrologiques calculés au cours de l'étude.
<br>

<div style="text-align:center">
<b>Une lecture de l'étude est conseillée pour appréhender et interpréter les indicateurs présentés dans cette application.</b>
</div>

<h3>Sélection des stations hydrométriques</h3>
<p>
La sélection des stations hydrométriques de l'étude s'est basée sur l'analyse conjointe de plusieurs critères permettant d’évaluer la robustesse des chroniques de débits journalier : 
<ul>
	<li><b>La validité des données qui est renseignée par l’organisme producteur de la donnée :</b> seules les stations affichant des données valides sur au moins 50 % ont été sélectionnées.</li>
	<li><b>L’emprise temporelle des chroniques disponibles :</b> il faut qu’elle soit suffisamment importante pour être représentative du régime hydrologique.</li>
	<li><b>La complétude des chroniques :</b> le nombre de données manquantes dans les chroniques doit être inférieur à un seuil fixé pour que la station soit retenue.</li>
</ul>
</p>

<h3>Calcul des classes du portail cartographique</h3>

Le portail cartographique présente les valeurs des indicateurs sous la forme de classe pour mettre en exergue les disparités régionales. Ces classes sont calculées automatiquement par l'application en s'appuyant sur les quantiles de la distribution.

<h3>Mise à jour</h3>

L'application s'appuie sur la base de données associée à l'étude. Dans la version actuelle de l'application, cette base de données n'est pas mise à jour.<br>

<h2>Les données utilisées</h2>

<h3>Données hydrologiques</h3>

Les données de hydrologiques de l'application proviennent d'une extraction sur le site <a href="https://hydro.eaufrance.fr/" target="_blank">https://hydro.eaufrance.fr/</a> réalisée le <code>24/02/2021</code> sur laquelle un ensemble d'indicateur a été calculé.
<br>
Si vous souhaitez avoir plus d'information sur la construction de ces indicateurs, vous pouvez consulter le rapport de l'étude.

<h3>Données géographiques</h3>

Les données géographiques des cartes dynamiques du site sont :
<p>
<ul>
	<li> Réseau hydrographique : le réseau hydrographique affiché sur les cartes correspond au <a href="https://geo.data.gouv.fr/fr/datasets/ee5c709c9b7ff928ab2529b79ce6e879c4de6950" target="_blank">réseau BD Carthage 2017/</a>. La géométrie du réseau a légèrement été simplifié pour des gains de performance,</li>
	<li> Contour des masses d'eau : Le contour des masses d'eau superficielles provient de l'état des lieux 2019 (<a href="https://geo.data.gouv.fr/fr/datasets/b28a0cdbbf507bbd08387dc7f5f49214a10aa59f" target="_blank">Source</a>). Pour des gains de performance, le contour a lui aussi été légèrement simplifié.</li>
</ul>
</p>


<h2>Contact</h2>

<a href="mailto:srnp.dreal-paysdelaloire@developpement-durable.gouv.fr">srnp.dreal-paysdelaloire@developpement-durable.gouv.fr</a>
</div>

