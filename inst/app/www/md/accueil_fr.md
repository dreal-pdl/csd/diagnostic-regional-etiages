<div class="page-title"><h1>Diagnostic Régional des Étiages</h1></div>

<div class="col-sm-12 pad">
<div>
<p>Ce site permet de consulter des indicateurs hydrologiques calculés à l'échelle des stations hydrométriques de la région Pays de la Loire pour appréhender les caractéristiques des étiages sur le territoire.<br>

C'est un outil complémentaire de l'étude financée par la DREAL pays de Loire<a href="https://www.pays-de-la-loire.developpement-durable.gouv.fr/etude-de-caracterisation-des-etiages-realisee-a6259.html" target="_blank"> sur l'influence des étiages sur la biologie des cours d'eau</a> qui vise à mieux comprendre les phénomènes d'étiages et d'assèchement :

<div style="text-align:center">
<ul>
	<li>Y a-t-il une aggravation des étiages en Pays de la Loire ?</li>
	<li>Quel est l'impact des étiages sur l’état écologique des cours d’eau ?</li>	
	<li>Quels sont les principaux facteurs à l'origine des assèchements constatés ?</li>	
</ul>
</div>

<div style="text-align:center">
<b>L'application reprend les principaux résultats de la première phase de l'étude qui vise à dresser un diagnostic de la sévérité des étiages sur le territoire régional.</b>
</div>

</p>
<img src="www/images/riviere.jpg" border="0" alt="rivière" class="illu-home">

</div>
<div class="exp">

<h3>Utilisation du site</h3>
Le tableau de bord permet d'interroger les données de l'étude sous forme de carte ou sous la forme de fiche station.
</div>
<div class="meas">

<a class="link-mea col-md-6 mea" href="tabActive1">
<h2>Cartographie régionale des indicateurs d'étiages</h2>
<img src="www/images/illu-carto.png" border="0" alt="rivière">
<p>Le portail cartographique permet d'appréhender la répartition spatiale des différents indicateurs. Pour chaque station, il est possible d'afficher via une liste déroulante, les indicateurs calculés en spécifiques (l/s/km2) afin de comparer les secteurs du territoire aux pas de temps interannuels (moyenne) et annuels (ex: 2003). Une fenêtre surgissante (pop-up) permet d’afficher l’évolution interannuelle de l'indicateur en cliquant sur une station.</p>
<span class="link-label"><span>J'y accède</span></span>
</a>


<a class="link-mea col-md-6 mea" href="tabActive2">
<h2>Indicateurs des étiages par station</h2>
<img src="www/images/illu-station.png" border="0" alt="rivière">
<p>Cette échelle permet d'accéder aux informations de contexte de la station hydrométrique sélectionnée et de visualiser l'évolution interannuelle des principaux indicateurs hydrologiques du cours de l'eau au droit de la station :
	<ul>
		<li>hydrologie générale</li>
		<li>hydrologie d'étiage</li>
		<li>hydrologie d'assèchement</li>
	</ul>
</p>
<span class="link-label"><span>J'y accède</span></span>
</a>
</div>
</div>