<div class="page-title"><h1>Mentions légales</h1></div>

<div class="pad">

<h2>Service gestionnaire</h2>

<p>Direction Régionale de l’Environnement de l’Aménagement et du Logement des Pays de la Loire
5, rue Françoise Giroud<br>
CS 16326<br>
44263 NANTES Cedex 2<br>
Tél : 02 72 74 73 00<br>
Fax : 02 72 74 73 09
</p>

<p>Courriel : <a href="mailto:dreal-paysdelaloire@developpement-durable.gouv.fr">dreal-paysdelaloire@developpement-durable.gouv.fr</a></p>

<h2>Directrice de publication</h2>

<p>Anne Beauval, directrice régionale de l’environnement, de l’aménagement et du logement des Pays de la Loire.</p>

<h2>Conception et réalisation</h2>

<p>Charte graphique, ergonomie : <a href="https://www.anteagroup.fr/services/gestion-donnees-environnementales-data DRI d'Antea Group" target="_blank"> DRI d'Antea Group</a></p>
<p>Développement : <a href="https://www.anteagroup.fr/services/gestion-donnees-environnementales-data DRI d'Antea Group" target="_blank"> DRI d'Antea Group</a>

<h2>Hébergement</h2>

<p>Rstudio - plateforme Shinyapps - <a href="http://shinyapps.io" target="_blank">http://shinyapps.io</a></p>


<h2>Droit d’auteur - Licence</h2>

<p>Tous les contenus présents sur le site de la direction régionale de l’Environnement, de l’Aménagement et du Logement des Pays de la Loire sont couverts par le droit d’auteur. Toute reprise est dès lors conditionnée à l’accord de l’auteur en vertu de l’article L.122-4 du Code de la Propriété Intellectuelle.</p>

<p>Toutes les informations liées à cette application (données et textes) sont publiées sous <a href="https://www.etalab.gouv.fr/wp-content/uploads/2017/04/ETALAB-Licence-Ouverte-v2.0.pdf" target="_blank">licence ouverte/open licence v2 (dite licence Etalab)</a> quiconque est libre de réutiliser ces informations, sous réserve notamment, d'en mentionner la filiation.</p>

<p>Toutes les scripts source de l'application sont disponibles sous licence GPL-v3.</p>

<h2>Code source</h2>

<p>L'ensemble des scripts de collecte et de datavisualisation est disponible sur le <a href="https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/diagnostic-regional-etiages/-/tree/main" target="_blank">https://gitlab-forge.din.developpement-durable.gouv.fr</a>. Vous pouvez y reporter les éventuels bugs ou demandes d'évolution au niveau de la rubrique Issue.</p>

<h2>Établir un lien</h2>
    <ul>
    <li>Tout site public ou privé est autorisé à établir, sans autorisation préalable, un lien vers les informations diffusées par le Ministère de la Transition écologique et solidaire et le Ministère de la Cohésion des Territoires.</li>

    <li>L’autorisation de mise en place d’un lien est valable pour tout support, à l’exception de ceux diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure porter atteinte à la sensibilité du plus grand nombre.</li>

    <li>Pour ce faire, et toujours dans le respect des droits de leurs auteurs, une icône “Marianne” est disponible ici pour agrémenter votre lien et préciser que le site d’origine est celui du Ministère de la Transition écologique et solidaire ou du Ministère de la Cohésion des Territoires.</li>
    </ul>

<h2>Usage</h2>

    <ul>
    <li>Les utilisateurs sont responsables des interrogations qu’ils formulent ainsi que de l’interprétation et de l’utilisation qu’ils font des résultats. Il leur appartient d’en faire un usage conforme aux réglementations en vigueur et aux recommandations de la CNIL lorsque des données ont un caractère nominatif (loi n° 78.17 du 6 janvier 1978, relative à l’informatique, aux fichiers et aux libertés dite loi informatique et libertés).</li>

    <li>Il appartient à l’utilisateur de ce site de prendre toutes les mesures appropriées de façon à protéger ses propres données et/ou logiciels de la contamination par d’éventuels virus circulant sur le réseau Internet. De manière générale, la Direction Régionale de l’Environnement de l’Aménagement et du Logement des Pays de la Loire décline toute responsabilité à un éventuel dommage survenu pendant la consultation du présent site. Les messages que vous pouvez nous adresser transitant par un réseau ouvert de télécommunications, nous ne pouvons assurer leur confidentialité.</li>
    </ul>
</div>