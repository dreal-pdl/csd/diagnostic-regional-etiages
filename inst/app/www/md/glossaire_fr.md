<div class="page-title"><h1>Glossaire</h1></div>

<div class="col-sm-12 pad" id="glossaire">
<div class="item">
    <div class="label">Assec :</div> 
    L’assèchement d’un cours d’eau correspond au cas le plus extrême de l’étiage : lorsqu’il n’y a plus d’écoulement et que le lit de la rivière est asséché (débit journalier nul).
</div>
<div class="item">
    <div class="label">Débit spécifique :</div> 
    Débit (m3/s) ramené à la surface du bassin versant réel de la station (km2). Ce rapport s’applique aux débits et aux indicateurs dérivés afin de comparer différents secteurs.
</div>
<div class="item">
    <div class="label">Débit d’Etiage Absolu (DEA) :</div> 
    Débit le plus faible mesuré sur la station hydrométrique.
</div>
<div class="item">
    <div class="label">Débit Caractéristique d’Etiage (DCR) :</div> 
    Débit égalé ou non dépassé 10 jours par an. C’est un débit caractéristique souvent utilisé pour caractériser l’étiage d’un cours d’eau.
</div>
<div class="item">
    <div class="label">Débit Moyen annuel (QMA) :</div> 
    Débit moyen calculé sur les chroniques journalières d’une année calendaire ou hydrologique
</div>
<div class="item">
    <div class="label">Débit moyen mensuel :</div> 
    Débit moyen de chaque mois d’une année calendaire. On parle de débit moyen mensuel interannuel lorsqu’il est calculé sur l’ensemble d’une chronique (ex : tous les mois de janvier).
</div>
<div class="item">
    <div class="label">Module (QA) :</div> 
    Débit moyen interannuel établi avec plusieurs années de mesures
</div>
<div class="item">
    <div class="label">Quantité Mensuelle miNimale Annuelle (QMNA) :</div> 
    Débit moyen mensuel le plus bas de l’année. 
</div>
<div class="item">
    <div class="label">Intensité d’étiage :</div> 
    Indicateur calculé en confrontant le QMNA au débit moyen annuel (QMA). Cet indicateur donne une représentation de la part que le représente le débit d’étiage (ici le QMNA) par rapport au régime hydrologique moyen du cours d’eau (le module). Compris entre 0 et 1 (ou en %), il caractérise un étiage extrêmement sévère lorsqu’il est proche de 0 et à l’inverse, un étiage peu sévère lorsque sa valeur se rapproche de 1.
</div>
<div class="item">
    <div class="label"> Coefficient de récession :</div> 
    Indicateur issu de la méthode d’analyse de la courbe de récession proposée par C. Catalogne (2012) qui représente la résilience d’un bassin versant en l’absence de précipitation. Plus sa valeur est faible, plus le bassin est résilient. A l’inverse, une valeur élevée indiquera une sensibilité du bassin en période de sécheresse. Cet indicateur s’analyse conjointement avec le temps de demi-vie définit plus bas.
</div>
<div class="item">
    <div class="label">RRSE : Réseau de référence sur la Surveillance des Etiages</div> 
    Échantillon des stations hydrométriques peu influencées et caractérisées par des chroniques robustes identifiées par l'INRAE (2010) pour l'étude des étiages à l'échelle métropolitaine.
</div>
<div class="item">
    <div class="label">Sévérité des étiages :</div> 
    La notion de sévérité est définie par rapport à des débits seuils identifiés dans la littérature scientifique. 3 niveaux de sévérité sont ainsi définis à partir du DC80 (débits franchi 20% du temps), du DCE (Débit caractéristique d'étiage) et du VCN3 décennale (VCN3 ayant une probabilité d'être observé 1 année sur 10).
</div>
<div class="item">
    <div class="label">Temps de demi-vie :</div> 
    Indicateur extrait de l’analyse de la courbe de récession <a href="https://hal.inrae.fr/tel-02597804/document" target="_blank">C. Catalogne,2012</a>. Il indique le temps nécessaire pour parcourir la moitié de la pente (vitesse de la première phase de récession). En d’autres termes, il renseigne sur la vitesse de vidange des stocks hydriques d’un hydrosystème.
</div>
<div class="item">
    <div class="label">Typologie d’étiage :</div> 
    Typologie des étiages déterminés à partir du rapport entre le nombre de jours moyen de franchissement des seuils de sévérité définis dans l’étude.
</div>
<div class="item">
    <div class="label">Volume Consécutif Minimal sur x jours (VCNx) :</div> 
    Débit minimal classiquement calculé sur 3, 7, 10 et 30 jours consécutif. Cet indicateur permet de caractériser un étiage sévère sur une courte période.
</div>
</div>