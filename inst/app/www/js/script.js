$(document).ready(function(){
  $(document).on('shiny:idle', function(event) {
      setTimeout(function() {
          initLayout();
      }, 500);
  });
  $("body").attr("content-active","content-tabActive0");
  $("body").addClass('open-details');
  $('.graph-details a.action-btn.open').html('<i class="fa fa-close"></i>');
})

function initLayout(){
  $('.annexe-page').hide();
  //Init info panel
  
  $("nav.navbar .navbar-nav li:visible a").unbind().click(function(e){
    e.preventDefault();
    $(".annexe-page").hide();
    let myActivePane = 'tabActive' + $(this).parent().index();
    
    if(myActivePane !="#"){
      $("body").attr("content-active","content-"+myActivePane);
    };

    //responsive
    if($(".navbar-collapse").hasClass("in")){
      $(".navbar-toggle").trigger('click');
    }

    $('html, body').animate({ scrollTop: 0 }, 500);
  })
  //Link close carto
  $('.carto-definition .close').click(function(e){
    $('.carto-definition').hide()
  })

  //Link mea in home
  $(".link-mea").unbind().click(function(e){
    e.preventDefault();
    let tabLink = parseInt($(this).attr('href').replace("tabActive",""));
    $("nav.navbar .navbar-nav li").eq(tabLink).find('a').trigger('click');
    $('html, body').animate({ scrollTop: 0 }, 500);
  })
  
  // Ouverture panneau lateral de la carto
  $(".graph-details a.action-btn.open").unbind().click(function(e){
    e.preventDefault();
    if($('body').hasClass('open-details')){
      $('body').removeClass('open-details');
      $('.graph-details a.action-btn.open').html('<i class="fa-solid fa-chart-bar"></i>');
    }else{
      $('body').addClass('open-details');
      $('.graph-details a.action-btn.open').html('<i class="fa fa-close"></i>');
    }
    
  })
  $('.widget-user-header h3 a').attr('title','Voir la fiche sur Hydro Portail')
  $('.widget-user-header h3 a').tooltip()

  //Add class on body when toggle-button clicked on responsive
  $(".navbar-toggle").unbind().click(function(e){
    e.preventDefault();
    if($(".navbar-collapse").hasClass('in')){
      $('body').removeClass('menu-opened');
    }else{
      $('body').addClass('menu-opened');
    }
  })

  // Liens annexe
  $(".annexe-link").unbind().click(function(e){
      e.preventDefault();
      $('html, body').animate({ scrollTop: 0 }, 500);
      let link = $(this).attr("href");
      let myActivePane = 'tabActiveAnnexe' + $(this).index();       
      if(myActivePane !="#"){
        $("body").attr("content-active","content-"+myActivePane);
      };
      $('a[data-toggle="tab"]').not('#synthese_station_ui_1-table_indicateurs a[data-toggle="tab"]').parent().removeClass('active');
      $('.tab-pane.active').not('#synthese_station_ui_1-table_indicateurs .tab-pane.active').removeClass('active');
      $(link).show();
  })

  // Redirection de la popup carto vers la fiche station
  $(".link-station").unbind().click(function(e){
    e.preventDefault();
    let cdstation=$(this).attr("href");
    $("nav.navbar .navbar-nav li").eq(2).find('a').trigger('click');
    $("#shiny-modal").modal('toggle');
    setTimeout(function(){
      $("#synthese_station_ui_1-choix_station").val(cdstation).change();
    },250);
  })

  $(window).resize(function () {
    $("#synthese_carto_ui_1-map").css('min-height',($(window).height()-218)+'px');
  }).resize();
}

function fitToscreen(element, minus = 0, overflow = false){
  $(element).each(function(){
    $(this).addClass('fitToscreen');
    var position = $(this).offset();
    if(position != undefined){
      var positionTop = position.top;
      $(this).css('min-height',($(window).height()-positionTop-minus)+'px');
      if(overflow == true){
        $(this).outerHeight($(window).height()-positionTop-minus);
      }
      //console.log($(this).height())
    }
  })
}
