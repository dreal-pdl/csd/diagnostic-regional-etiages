-- Vue matérialisée dédiée à l'application

DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.indicateur_station;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.indicateur_station AS 
SELECT cdstation,"module_QA" as valeur,'qa' as indicateur
FROM diagnostic_regional_etiages.module
UNION
SELECT cdstation,"DCE" as valeur,'dce' as indicateur
FROM diagnostic_regional_etiages.debits_caracteristiques
UNION
SELECT cdstation,"etiage_abs" as valeur,'dea' as indicateur
FROM diagnostic_regional_etiages.debits_caracteristiques
UNION 
SELECT cdstation, "val_th" as valeur,'qmna5' as indicateur
FROM diagnostic_regional_etiages.qmna5_sec
order by indicateur,cdstation
WITH DATA;
CREATE INDEX ON diagnostic_regional_etiages.indicateur_station (indicateur,cdstation);

-- Liste des stations
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.liste_station_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.liste_station_epsg4326 AS 
SELECT distinct sgeom.cdstation,
et.date_debut,et.annee_debut,et.mois_debut,et.date_fin,et.annee_fin,et.mois_fin,et.emprise_temp_annee,
et.nb_annee,et.classe,
sgeom.nom_statio as nom_station,sgeom.type_stati,sgeom.date_mserv as date_mservice,sgeom.date_ferme as date_fermeture,
sgeom.cdsecteurh,sgeom.cdzonehyd,
sgeom.cdtronconh,sgeom.cdentitehy,sgeom.coordxstat,sgeom.coordystat,ST_Transform(sgeom.geometry,4326) as geom ,sgeom.perc,
s.statut_station,s.finalite_station,s.influence,s.altitude,s.surface_bv_reel,s.surface_bv_topo,
s.qualite_basses_eaux,s.loi_basses_eaux,s.qualite_moy_eaux,s.qualite_hautes_eaux,s.loi_module,"RRSE",
sh.surface_bv,
me.cdbvspemdo, me.nombvspemd,
e.ecolo_etat, e.ecolo_conf, e.chim_etat, e.chim_ss_ubiq_etat, e.cdmassrapp,
ps.r_pponct,ps.r_pdiff,ps.r_hydro,ps.r_morpho, ps.r_micpol_eco, ps.r_micpol_eco_ssubi, ps.r_micpol_chim_ssubi,concat_ws(' : ',sgeom.cdstation,sgeom.nom_statio) as label,
typo_etiage.typologie_severite
FROM diagnostic_regional_etiages.module as m
LEFT JOIN 
(
SELECT *
FROM indicateur_station_hydro.emprise_temporelle_chronique_complete
where (cdstation NOT IN ('M3403010') and date_debut NOT IN ('1995-01-02'))
) et on et.cdstation=m.cdstation
RIGHT JOIN reseau_suivi.station_hydro_intersect_pdl_epsg2154 as sgeom ON sgeom.cdstation=m.cdstation
LEFT JOIN reseau_suivi.fiche_station as s ON s.cdstation=m.cdstation
LEFT JOIN reseau_suivi.fiche_station_hubeau as sh ON sh.code_site=m.cdstation
LEFT JOIN reseau_suivi.lien_stationh_bvme_nogeom as me ON me.cdstation=m.cdstation
LEFT JOIN edl_dce.etat_niveau1_rwbody_nogeom as e on e.cdmassrapp=me.cdbvspemdo
LEFT JOIN edl_dce.pression_significative_niveau1_rwbody_nogeom as ps on ps.eu_cd_rap=e.cdmassrapp
LEFT JOIN hydrologie.pdll_typologie_etiage as typo_etiage on typo_etiage.cdstation=m.cdstation
;
CREATE INDEX ON diagnostic_regional_etiages.liste_station_epsg4326 (cdstation);

-- reseau_hydro_geom
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.reseau_hydro_2017_sandre_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.reseau_hydro_2017_sandre_epsg4326 AS 
select oid,id,gid,cdentitehy,nomentiteh,candidat,classe, ST_Transform(st_simplifypreservetopology(geom, 75::double precision),4326) as geom
from contexte_physique.reseau_hydro_2017_sandre_epsg2154
WHERE classe <=4
WITH DATA;
CREATE INDEX id_geom_reseau ON diagnostic_regional_etiages.reseau_hydro_2017_sandre_epsg4326 USING gist (geom);

-- contour hydro
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.perimetre_hydro_fusion_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.perimetre_hydro_fusion_epsg4326 AS 
select oid,id,ST_Transform(st_simplifypreservetopology(geom, 100::double precision),4326) as geom
from administratif.perimetre_hydro_fusion_epsg2154
WITH DATA;
CREATE INDEX id_geom_perimetre ON diagnostic_regional_etiages.perimetre_hydro_fusion_epsg4326 USING gist (geom);

-- bv_masse_eau_epsg4326
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.bv_masse_eau_epsg4326;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.bv_masse_eau_epsg4326 AS 
SELECT oid, id, cdbvspemdo, nombvspemd, niveauprec, surfacebvs, cdcategori, cdmassedea, commentbvs, ST_MakeValid(ST_Buffer(ST_Buffer(ST_Transform(st_simplifypreservetopology(geom, 100::double precision),4326),-0.001),0.001)) as geom
FROM edl_dce.bv_masse_eau_epsg2154
WITH DATA;
CREATE INDEX id_geom_meso ON diagnostic_regional_etiages.bv_masse_eau_epsg4326 USING gist (geom);

-- diagnostic_regional_etiages.pdll_indicateurs_recession_stations_retenues_nogeom
DROP MATERIALIZED VIEW IF EXISTS diagnostic_regional_etiages.pdll_indicateurs_recession_stations_retenues_nogeom;
CREATE MATERIALIZED VIEW diagnostic_regional_etiages.pdll_indicateurs_recession_stations_retenues_nogeom AS 
SELECT cdstation, alpha, coefficient_recession, demie_d
FROM hydrologie.pdll_indicateurs_recession_stations_retenues_nogeom
WITH DATA;
